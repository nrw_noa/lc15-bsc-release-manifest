NuRAN Wireless LiteCell 1.5 BSC release notes

Version 1.1.2.0
NuRAN Wireless


Version 1.1.2.0[ 07/20/2017 ]
-------------------------------------------------------------------------------------------------
Additions include:

- NITB	
	- Created NITB release
	- Added OpenBSC, OSMO-NITB, OSMO-GSN, Freeswitch to NitB release
	- Support of FR, HR and AMR* codecs with DTX
	- Support of G.711u, G.711a and G.729* codecs in PBX

- BTS	
	- Modified bicolor LED indication priorities

- BSP	
	- Added an option to limit the throughput while downloading Yocto packages 
	- Added a method to modify systemd unit files that will be preserved even after a monolitic update
	- Added BSP needed flash configuration softlink for openvpn client/server
	- Added recovery boot (serial boot) and complete rootfs recovery support
	- Added support different SD card sizes
	- Added monolithicupdate/initsu download bandwidth limit option
	- Added Python 3 support

Bugs that have been addressed in this release include:

- NITB: 
	- None

- BTS: 
	- Fixed osmo-bts GSM-850 band selection in auto-band mode.

- BSP: 
	- Improved reliability of update per package mechanism
	- Corrected watchdog interrupt fail triggered reboot

Pending issues and limitations:

- NITB: 
	- AMR codec support must be manually enabled in Freeswitch config file

- BTS:
	- PCU supports either GPRS Only or EDGE only. It does not support both modes simultaneously
   
- BSP:
	- None



